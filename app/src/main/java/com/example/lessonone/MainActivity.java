package com.example.lessonone;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //taskOne

        final TextView textHello = findViewById(R.id.textHello);

        Button buttonSetText = findViewById(R.id.buttonSetHelloWorld);
        Button buttonClear = findViewById(R.id.buttonClear);

        buttonSetText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textHello.setText("Привет, мир!");
            }
        });

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textHello.setText("");
            }
        });

        //Task Two

        final TextView textCounter = findViewById(R.id.textCounter);

        Button buttonMinus = findViewById(R.id.buttonMinus);
        Button buttonPlus = findViewById(R.id.buttonPlus);

        final String[] counter = {"1"};
        textCounter.setText(counter[0]);

        buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = Integer.parseInt(counter[0]) - 1;
                counter[0] = Integer.toString(i);
                textCounter.setText(counter[0]);
            }
        });

        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = Integer.parseInt(counter[0]) + 1;
                counter[0] = Integer.toString(i);
                textCounter.setText(counter[0]);
            }
        });
    }
}
